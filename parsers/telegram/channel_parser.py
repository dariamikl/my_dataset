from telethon import TelegramClient, sync
from telethon import utils
import datetime
import json 

API_ID = 119992 
API_HASH = '6bac7984661a330b8ee234544b1ad4fc'

client = TelegramClient('session', API_ID, API_HASH).start()
jun1 = datetime.datetime(2018, 7, 1, 11, 55, 45, tzinfo=datetime.timezone.utc)
# Oct 18 2018 - Jan 16 2019
# https://tlgrm.ru/channels/news
# 
# @ia_panorama
# +@lentaruchannel
# @bbcrussian
# +@vedomosti
# @vcnews
# @lifenews
# @znakcom
# @meduza_news
# @mediazzzona
# @rtvimain
# @radiosvoboda
# @kommersant


def download_tg_messages(chan):
	messages = client.iter_messages(f'@{chan}', wait_time=1)

	acc = []
	i = 0
	print('\n')
	print(f'Downloading - @{chan}')
	for m in messages:
		if m.date < jun1:
			break

		acc.append(m)
		i += 1
		if i % 10 == 0:
			print(i, m.date)

	text_date_list = []

	for m in acc:
		text_date_list.append({'text': m.text, 'date': m.date.ctime()})

	fp = open(f'channel_parsed_json/{chan}.tg.json', 'w')

	json.dump(text_date_list, fp)
	print(f'Done - @{chan}')
	print('\n')


media_channels = [ 'ia_panorama', 'lentaruchannel', 'bbcrussian', 'vedomosti', 'vcnews', 'lifenews', 'znakcom', 'meduza_news', 'mediazzzona', 'rtvimain', 'radiosvoboda', 'kommersant' ] 

for chan in media_channels: 
	download_tg_messages(chan)

# urls = []
# errors = []

# for m in messages:
# 	try:
# 		urls.append(m.media.webpage.url)
# 	except:
# 		try:
# 			length, offset = m.entities[0].length, m.entities[0].offset
# 			urls.append( m.text[offset:offset+length] )
# 		except:
# 			print('no url')
