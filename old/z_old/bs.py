import os
import json
from bs4 import BeautifulSoup

files = os.listdir('htmls')

jsons = []
problems = []

for filename in files:
	try:
		with open(f'htmls/{filename}') as f:
			html = f.read()
			soup = BeautifulSoup(html, 'lxml')

			h1 = soup.body.h1.get_text()
			cat = soup.find('span', {'class':'category-button'}).a.get_text()
			posted = soup.find('time', {'class':'entry-date published'}).attrs['datetime']
			content = soup.find('div', {'class': 'entry-content'}).get_text()

			jsons.append({'title': h1, 'category': cat, 'date': posted, 'body': content})
	except Exception as e:
		print(e, filename)
		problems.append(filename)


with open('articles.json', 'w') as f:
	json.dump(jsons, f)