import os
import json
import re

lentaruchannel = json.load(open('../channel_parsed_json/lentaruchannel.tg.json'))

pattern = r'https://[\w_-]+(?:(?:\.[\w_-]+)+)[\w.,@?^=%&:/~+#-]*[\w@?^=%&/~+#-]?'

re.findall(pattern, lentaruchannel[0]['text'])

urls = []

for v in lentaruchannel:
    try:
        urls.extend(re.findall(pattern, v['text']))
    except:
        print(v)

urls = list(filter(lambda u: 'https://lenta.ru/articles' not in u, urls))

with open('../urls/lentaruchannel.txt','w') as f:
    f.write('\n'.join(urls))