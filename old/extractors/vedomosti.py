import os
import json
import re
import requests
from tqdm import tqdm

vedomosti = json.load(open('channel_parsed_json/vedomosti.tg.json'))

pattern = r'vdmsti\.ru\/[a-zA-Z0-9]*'

re.findall(pattern, vedomosti[0]['text'])

short_urls = []

for v in vedomosti:
    try:
        short_urls.extend(re.findall(pattern, v['text']))
    except:
        print(v)

urls = []

for url in tqdm(short_urls):
    u = requests.get(f"https://{url}").url
    urls.append(u)


with open('urls/vedomosti.txt','w') as f:
    f.write('\n'.join(urls))