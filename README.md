## Project structure

`/parsers` - top-level package that accumulates prarsing functionality
  - `telegram` - Telegram channel parser. One of the tricks is that media usually have Telegram Channels in which they broadcast posts. Telegram Client Library [**Telethon**](https://telethon.readthedocs.io/en/latest/) allows to execute scripts on behalf of the user and not the bot, i.e join channels, send messages as if the bot was a real user. 
    
    Telethon also allows to extract entities (e.g. URL) from the messages. These URLs should be then matched against previouslt discovered regex pattern, e.g. IA Panorama's article URL has a pattern of `https://panorama.pub/<int>-<text slug>.html`. 
    
    This module can be implemented as a standalone script that would take a pattern and a Telegram channel as input parameters and will output (STDOUT and then to a file) the desired and matched URLs that can be directed to `/data/raw_telegram/<channel name>.txt`.

    Later, these files will be sent to `wget` that will spit raw html files to `/data/raw_html/<channel name>/<url>.html` files that will later be a source for html parsers.
  
  - `html` - HTML parsers. Every media, obviously, has its distinct html stucture, so it would require to write custom `bs4` powered packages, i.e. `/parsers/html/ia_panorama.py` that will read `/data/raw_html/ia_panorama/*.html` files and extract desired information such as article's __title__, __body__, __publication date__, __category__, __source (url)__ and save this to a single combined CSV file at `/data/raw_csv/<channel name>.csv`